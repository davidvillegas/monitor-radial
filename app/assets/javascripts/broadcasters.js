// # Place all the behaviors and hooks related to the matching controller here.
// # All this logic will automatically be available in application.js.
// # You can use CoffeeScript in this file: http://coffeescript.org/

document.addEventListener("turbolinks:load", function() {

  function validateradioForm() {
    $("#radioform").validate({
      debug: false,
      rules: {
        "broadcaster[name]": "required",//{ minlength: 10, maxlength: 100 }
        "broadcaster[email]": {
          required: true,
          email: true
        },
        "broadcaster[location]": "required",
        "broadcaster[ref]": "required",
        "broadcaster[phone]": "required",
        "broadcaster[contact_name]": "required"
      }
    })
  }
  $(document).ready(validateradioForm);
  $(document).on('page:load', validateradioForm);

})
