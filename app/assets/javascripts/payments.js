// # Place all the behaviors and hooks related to the matching controller here.
// # All this logic will automatically be available in application.js.
// # You can use CoffeeScript in this file: http://coffeescript.org/

document.addEventListener("turbolinks:load", function() {

  function validatepaymentForm() {
    $("#paymentform").validate({
      debug: false,
      rules: {
        "payment[invoice]": "required"
             }
          })
        }
  $(document).ready(validatepaymentForm);
  $(document).on('page:load', validatepaymentForm);

})
