class AdminsController < ApplicationController
  before_action :authenticate_user!
  before_action :authenticate_admin
  layout :resolve_layout
  def index
  end

  def manageusers
    @users = User.where(role: 'regular')

  end

  private

  def authenticate_admin
    if current_user.role != 'admin'
      redirect_back fallback_location: root_path, alert: "No posee los permisos necesarios"
    end
  end

  def resolve_layout
    case action_name
    when "manageusers"
      "admin"
    end
  end

end
