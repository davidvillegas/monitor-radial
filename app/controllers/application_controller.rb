class ApplicationController < ActionController::Base
  # before_action :authenticate_user!
  before_action :configure_sign_up_params, if: :devise_controller?

  protected

	def configure_sign_up_params
	  devise_parameter_sanitizer.permit(:sign_up, keys: [:name])
 	end

  def after_sign_up_path_for(resource)
    billboard_path
  end

  def after_sign_in_path_for(resource)
    @user = current_user
    if current_user.role == 'admin'
      administration_path
    elsif (current_user.role == 'regular') && (@user.status == 'activo')
      billboard_path
    else
      root_path
    end
  end

end
