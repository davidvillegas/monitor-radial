class BillboardsController < ApplicationController
  before_action :authenticate_user!
  layout :resolve_layout
  def index
    @user = current_user
  end

  def top_100
    @users = User.all
    @user = current_user
    @top100 = Song.all.where(fecha: DateTime.now - 7..DateTime.now - 1).group(:titulo, :interprete).order('COUNT(titulo) DESC').limit(100)
    @lastweek = Song.all.where(fecha: DateTime.now - 14..DateTime.now - 8).group(:titulo, :interprete).order('COUNT(titulo) DESC').limit(100)
  end

  def top_100_pdf
    @top100 = Song.all.where(fecha: DateTime.now - 7..DateTime.now - 1).group(:titulo, :interprete).order('COUNT(titulo) DESC').limit(100)
    @lastweek = Song.all.where(fecha: DateTime.now - 14..DateTime.now - 8).group(:titulo, :interprete).order('COUNT(titulo) DESC').limit(100)
    render pdf: 'top_100_pdf.pdf', layout: 'pdf.html'
  end


  def top_latino
    @users = User.all
    @user = current_user
    @toplatino = Song.all.where(fecha: DateTime.now - 7..DateTime.now - 1, genero: 'latino').group(:titulo, :interprete).order('COUNT(titulo) DESC').limit(100)
    @lastweek = Song.all.where(fecha: DateTime.now - 14..DateTime.now - 8, genero: 'latino').group(:titulo, :interprete).order('COUNT(titulo) DESC').limit(100)
  end

  def top_latino_pdf
    @toplatino = Song.all.where(fecha: DateTime.now - 7..DateTime.now - 1, genero: 'latino').group(:titulo, :interprete).order('COUNT(titulo) DESC').limit(100)
    @lastweek = Song.all.where(fecha: DateTime.now - 14..DateTime.now - 8, genero: 'latino').group(:titulo, :interprete).order('COUNT(titulo) DESC').limit(100)
    render pdf: 'top_latino_pdf.pdf', layout: 'pdf.html'
  end

  def top_tradicional
    @users = User.all
    @user = current_user
    @toptradicional = Song.all.where(fecha: DateTime.now - 7..DateTime.now - 1, genero: 'tradicional').group(:titulo, :interprete).order('COUNT(titulo) DESC').limit(100)
    @lastweek = Song.all.where(fecha: DateTime.now - 14..DateTime.now - 8, genero: 'tradicional').group(:titulo, :interprete).order('COUNT(titulo) DESC').limit(100)
  end

  def top_tradicional_pdf
    @toptradicional = Song.all.where(fecha: DateTime.now - 7..DateTime.now - 1, genero: 'tradicional').group(:titulo, :interprete).order('COUNT(titulo) DESC').limit(100)
    @lastweek = Song.all.where(fecha: DateTime.now - 14..DateTime.now - 8, genero: 'tradicional').group(:titulo, :interprete).order('COUNT(titulo) DESC').limit(100)
    render pdf: 'top_tradicional_pdf.pdf', layout: 'pdf.html'
  end

  def top_balada
    @users = User.all
    @user = current_user
    @topbalada = Song.all.where(fecha: DateTime.now - 7..DateTime.now - 1, genero: 'balada').group(:titulo, :interprete).order('COUNT(titulo) DESC').limit(100)
    @lastweek = Song.all.where(fecha: DateTime.now - 14..DateTime.now - 8, genero: 'balada').group(:titulo, :interprete).order('COUNT(titulo) DESC').limit(100)
  end

  def top_balada_pdf
    @topbalada = Song.all.where(fecha: DateTime.now - 7..DateTime.now - 1, genero: 'balada').group(:titulo, :interprete).order('COUNT(titulo) DESC').limit(100)
    @lastweek = Song.all.where(fecha: DateTime.now - 14..DateTime.now - 8, genero: 'balada').group(:titulo, :interprete).order('COUNT(titulo) DESC').limit(100)
    render pdf: 'top_balada_pdf.pdf', layout: 'pdf.html'
  end

  def top_salsa
    @users = User.all
    @user = current_user
    @topsalsa = Song.all.where(fecha: DateTime.now - 7..DateTime.now - 1, genero: 'salsa').group(:titulo, :interprete).order('COUNT(titulo) DESC').limit(100)
    @lastweek = Song.all.where(fecha: DateTime.now - 14..DateTime.now - 8, genero: 'salsa').group(:titulo, :interprete).order('COUNT(titulo) DESC').limit(100)
  end

  def top_salsa_pdf
    @topsalsa = Song.all.where(fecha: DateTime.now - 7..DateTime.now - 1, genero: 'salsa').group(:titulo, :interprete).order('COUNT(titulo) DESC').limit(100)
    @lastweek = Song.all.where(fecha: DateTime.now - 14..DateTime.now - 8, genero: 'salsa').group(:titulo, :interprete).order('COUNT(titulo) DESC').limit(100)
    render pdf: 'top_salsa_pdf.pdf', layout: 'pdf.html'
  end

  def top_pop
    @users = User.all
    @user = current_user
    @toppop = Song.all.where(fecha: DateTime.now - 7..DateTime.now - 1, genero: 'pop').group(:titulo, :interprete).order('COUNT(titulo) DESC').limit(100)
    @lastweek = Song.all.where(fecha: DateTime.now - 14..DateTime.now - 8, genero: 'pop').group(:titulo, :interprete).order('COUNT(titulo) DESC').limit(100)
  end

  def top_pop_pdf
    @toppop = Song.all.where(fecha: DateTime.now - 7..DateTime.now - 1, genero: 'pop').group(:titulo, :interprete).order('COUNT(titulo) DESC').limit(100)
    @lastweek = Song.all.where(fecha: DateTime.now - 14..DateTime.now - 8, genero: 'pop').group(:titulo, :interprete).order('COUNT(titulo) DESC').limit(100)
    render pdf: 'top_pop_pdf.pdf', layout: 'pdf.html'
  end

  def top_rock
    @users = User.all
    @user = current_user
    @toprock = Song.all.where(fecha: DateTime.now - 7..DateTime.now - 1, genero: 'rock').group(:titulo, :interprete).order('COUNT(titulo) DESC').limit(100)
    @lastweek = Song.all.where(fecha: DateTime.now - 14..DateTime.now - 8, genero: 'rock').group(:titulo, :interprete).order('COUNT(titulo) DESC').limit(100)
  end

  def top_rock_pdf
    @toprock = Song.all.where(fecha: DateTime.now - 7..DateTime.now - 1, genero: 'rock').group(:titulo, :interprete).order('COUNT(titulo) DESC').limit(100)
    @lastweek = Song.all.where(fecha: DateTime.now - 14..DateTime.now - 8, genero: 'rock').group(:titulo, :interprete).order('COUNT(titulo) DESC').limit(100)
    render pdf: 'top_rock_pdf.pdf', layout: 'pdf.html'
  end

  def top_emisoras
    # ESTE QUERY DEBE SER DIFERENTE
    @users = User.all
    @user = current_user
    @topemisoras = Song.all.where(fecha: DateTime.now - 7..DateTime.now - 1, genero: 'emisoras').group(:titulo, :interprete).order('COUNT(titulo) DESC').limit(100)
    @lastweek = Song.all.where(fecha: DateTime.now - 14..DateTime.now - 8, genero: 'emisoras').group(:titulo, :interprete).order('COUNT(titulo) DESC').limit(100)
  end

  def top_emisoras_pdf
    # ESTE QUERY DEBE SER DIFERENTE
    @topemisoras = Song.all.where(fecha: DateTime.now - 7..DateTime.now - 1, genero: 'emisoras').group(:titulo, :interprete).order('COUNT(titulo) DESC').limit(100)
    @lastweek = Song.all.where(fecha: DateTime.now - 14..DateTime.now - 8, genero: 'emisoras').group(:titulo, :interprete).order('COUNT(titulo) DESC').limit(100)
    render pdf: 'top_emisoras_pdf.pdf', layout: 'pdf.html'
  end
  private

  def resolve_layout
    case action_name
    when "top_emisoras", "top_rock", "top_pop", "top_salsa", "top_balada", "top_latino", "top_tradicional", "top_100"
      "animation"
    end
  end

end
