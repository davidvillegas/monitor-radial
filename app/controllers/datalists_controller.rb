class DatalistsController < ApplicationController
  before_action :authenticate_user!
  before_action :authenticate_admin
  layout :resolve_layout
  # def index
  #   @datalists = Datalist.all
  # end

  def new
    @datalist = Datalist.new()
  end

  def create
    @datalist = Datalist.new(datalist_params)
    respond_to do |format|
      if @datalist.save
        format.html { redirect_to new_datalist_path, notice: '¡ Lista guardada correctamente!' }
      else
        format.html { redirect_to new_datalist_path, alert: '¡ Formato de archivo incorrecto. Intente nuevamente!'  }
      end
    end
  end

  # def show
  #   @datalist = Datalist.find(datalist_params)
  # end

  private

  def authenticate_admin
    if current_user.role != 'admin'
      redirect_back fallback_location: root_path, alert: "No posee los permisos necesarios"
    end
  end

  def datalist_params
    params.require(:datalist).permit(:list_date, :list)
  end

  def resolve_layout
    case action_name
    when "new"
      "admin"
    end
  end

end
