class HomeController < ApplicationController
  def index
    # @genres = Genre.all
    @popgenre = Genre.find_by('name like ?', '%pop%')
    @top100genre = Genre.find_by('name like ?', '%top 100%')
    @latinogenre = Genre.find_by('name like ?', '%latino%')
    @tradicionalgenre = Genre.find_by('name like ?', '%tradicional%')
    @baladagenre = Genre.find_by('name like ?', '%balada%')
    @salsagenre = Genre.find_by('name like ?', '%salsa%')
    @rockgenre = Genre.find_by('name like ?', '%rock%')
    @emisorasgenre = Genre.find_by('name like ?', '%emisoras%')
    @top40 = Song.all.where(fecha: DateTime.now - 7..DateTime.now - 1).group(:titulo, :interprete).order('COUNT(titulo) DESC').limit(40)
    @lastweek = Song.all.where(fecha: DateTime.now - 14..DateTime.now - 8).group(:titulo, :interprete).order('COUNT(titulo) DESC').limit(40)
  end

end
