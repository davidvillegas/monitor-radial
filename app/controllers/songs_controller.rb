class SongsController < ApplicationController
  before_action :authenticate_user!
  before_action :authenticate_admin
  layout :resolve_layout

  def create
    if Song.import(params[:file])
    redirect_to new_song_path, notice: "Lista de canciones agregadas correctamente!"
    else
      redirect_to new_song_path, alert: " ERROR! Lista de canciones no agregadas. Revise el formato del archivo"
    end
  end

  private
  def authenticate_admin
    if current_user.role != 'admin'
      redirect_back fallback_location: root_path, alert: "No posee los permisos necesarios"
    end
  end


  def resolve_layout
    case action_name
    when "new"
      "admin"
    end
  end
end
