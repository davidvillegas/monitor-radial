class SubscriptionController < ApplicationController
  def index
    if current_user.present?
      # Aca se debe redirigir al path de mi cuenta
      redirect_back fallback_location: root_path, alert: "Usted ya esta autenticado"
    end
  end
end