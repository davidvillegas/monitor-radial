class Broadcaster < ApplicationRecord
  validates :name, :email, :phone, :ref, :location, :contact_name, presence: true
  validates :name, uniqueness: true
end
