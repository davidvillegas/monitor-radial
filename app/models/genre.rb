class Genre < ApplicationRecord
  validates :name, presence: true
  validates :name, uniqueness: true
  has_one_attached :picture
  validate :correct_file_type

  private

  def correct_file_type
    if picture.attached? && !picture.content_type.in?(%w(image/jpg image/jpeg image/png))
      errors.add(:picture, 'Seleccionar solo archivos con extensión .jpg / .jpeg / .png')
    end
  end

end
