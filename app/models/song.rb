class Song < ApplicationRecord
  require 'csv'



  def self.import(file)
    if file.path.split('.').last.to_s.downcase == 'csv'
      self.check_file(file)
    elsif file.path.split('.').last.to_s.downcase == 'txt'
      self.check_file(file)
    else
      return false
    end
  end

  private


  def self.check_file(file)
    CSV.foreach(file.path, headers: true, encoding: 'iso-8859-1:utf-8', col_sep: ";") do |row|
      if (row.headers[0] != 'interprete') || (row.headers[1] != 'titulo') | (row.headers[2] != 'genero')  || (row.headers[3] != 'sello')
        return false
      else
        if row.length > 4
          return false
        else
          if row[0].blank? || row[1].blank? || row[2].blank? || row[3].blank?
            return false
          else
            #COMANDO A DEVELOPMENT
            # row['fecha'] = Time.now.strftime('%Y-%m-%d')
            #COMANDO PARA LA PRESENTACIÓN
            row['fecha'] = Faker::Date.between(15.days.ago, 1.days.ago)
            Song.create! row.to_hash
          end
        end
      end
    end
    return true
  end


end
