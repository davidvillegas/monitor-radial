class CreateBroadcasters < ActiveRecord::Migration[5.2]
  def change
    create_table :broadcasters do |t|
      t.string :name, null: false
      t.string :email, null: false
      t.string :phone, null: false
      t.string :ref, null: false
      t.string :location, null: false
      t.string :contact_name, null: false

      t.timestamps
    end
  end
end
