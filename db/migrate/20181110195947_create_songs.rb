class CreateSongs < ActiveRecord::Migration[5.2]
  def change
    create_table :songs do |t|
      t.string :interprete
      t.string :titulo
      t.string :genero
      t.string :sello
      t.date :fecha

      t.timestamps
    end
  end
end
