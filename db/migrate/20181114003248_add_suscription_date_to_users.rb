class AddSuscriptionDateToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :suscription_date, :datetime
  end
end
