# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


User.create([{email: 'admin@monitor-radial.com',password: '4dm1nm0n1t0r',role: 'admin', status: 'activo', name: 'Administrador'},
						{email: 'test@monitor-radial.com',password: '73s73r',role: 'regular', status: 'inactivo', name: 'Tester'},
						{email: 'vip@monitor-radial.com', password: 'n3wus3r', role: 'regular', status: 'activo', name: 'VIP', suscription_date: Time.now.strftime('%Y-%m-%d')}])

Genre.create([{name: 'top 100'},
							{name: 'pop'},
							{name: 'latino'},
							{name: 'tradicional'},
							{name: 'balada'},
							{name: 'salsa'},
							{name: 'rock'},
							{name: 'emisoras'}])

						# 	100.times do |index|
						# 		Song.create!([{interprete: Faker::Music.band, genero: Faker::Music.genre, titulo: Faker::Music.album, sello: Faker::Music.chord, fecha: Time.now.strftime('%Y-%m-%d')}])
						# end
