namespace :suscripcion do
  desc "TODO"
  task desactivar: :environment do
    activeusers = User.all.where(status: 'activo', role: 'regular')
    activeusers.each do |user|
      diferencia = Time.now - user.suscription_date
      if diferencia > 2592000
        user.status = 'inactivo'
      end
      user.save
    end
  end
end
